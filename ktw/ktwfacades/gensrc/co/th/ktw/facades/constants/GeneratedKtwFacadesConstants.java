/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Aug 7, 2017 3:02:40 PM                      ---
 * ----------------------------------------------------------------
 */
package co.th.ktw.facades.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedKtwFacadesConstants
{
	public static final String EXTENSIONNAME = "ktwfacades";
	
	protected GeneratedKtwFacadesConstants()
	{
		// private constructor
	}
	
	
}
