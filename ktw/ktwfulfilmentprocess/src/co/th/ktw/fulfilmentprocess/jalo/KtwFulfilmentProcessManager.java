/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package co.th.ktw.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import co.th.ktw.fulfilmentprocess.constants.KtwFulfilmentProcessConstants;

@SuppressWarnings("PMD")
public class KtwFulfilmentProcessManager extends GeneratedKtwFulfilmentProcessManager
{
	public static final KtwFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (KtwFulfilmentProcessManager) em.getExtension(KtwFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
