/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Aug 7, 2017 3:02:40 PM                      ---
 * ----------------------------------------------------------------
 */
package co.th.ktw.test.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedKtwTestConstants
{
	public static final String EXTENSIONNAME = "ktwtest";
	
	protected GeneratedKtwTestConstants()
	{
		// private constructor
	}
	
	
}
