/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Aug 7, 2017 3:02:40 PM                      ---
 * ----------------------------------------------------------------
 */
package co.th.ktw.initialdata.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedKtwInitialDataConstants
{
	public static final String EXTENSIONNAME = "ktwinitialdata";
	
	protected GeneratedKtwInitialDataConstants()
	{
		// private constructor
	}
	
	
}
